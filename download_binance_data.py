from datetime import datetime

import requests
import zipfile
import io
import pandas as pd
from pandas import date_range, to_datetime


base_url = 'https://data.binance.vision'
header = ['Open time', 'Open', 'High', 'Low', 'Close', 'Volume', 'Close time', 'Quote asset volume', 'Number of trades',
          'Taker buy base asset volume', 'Taker buy quote asset volume', 'Ignore']
symbol = 'BTCUSDT'
links = [f'{base_url}/data/spot/monthly/klines/{symbol}/1d/{symbol}-1d-{date:%Y-%m}.zip' for date in
         date_range(datetime(2018, 1, 1), datetime(2021, 12, 31), freq="M")]


if __name__ == '__main__':

    list_of_dfs = []
    for link in links:
        try:
            print(f"Downloading {link}")
            r = requests.get(link)
            if r.status_code != 404:
                z = zipfile.ZipFile(io.BytesIO(r.content))

                df = pd.read_csv(z.open(z.namelist()[0]), header=None)
                df[0] = to_datetime(df[0], unit='ms')
                list_of_dfs.append(df)
        except Exception as ex:
            print(f'Error for {link}. Message: {ex}')

    final_df = pd.concat(list_of_dfs)
    final_df.columns = header
    final_df = final_df.drop_duplicates(subset=["Open time"], keep='first')
    final_df.to_csv(rf'{symbol}.csv')
